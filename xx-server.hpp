#include "generated.hpp"

class Tester : public com::reverendhomer::Tester_adaptor,
               public DBus::IntrospectableAdaptor,
               public DBus::ObjectAdaptor,
               public DBus::PropertiesAdaptor
{
public:
    Tester(DBus::Connection &connection, const std::string &path);

    virtual std::string Prints() override;

    virtual void on_set_property(DBus::InterfaceAdaptor &interface,
                                 const std::string &property,
                                 const DBus::Variant &value) override;
};

class TestSuite : public com::reverendhomer::TestSuite_adaptor,
                  public DBus::IntrospectableAdaptor,
                  public DBus::ObjectAdaptor
{
private:
    DBus::Connection &m_Connection;
    std::vector<Tester> m_Workers;
public:
    TestSuite(DBus::Connection &connection);

    virtual std::string AddCon(const std::string &suffix) override;
};

