#include "xx-server.hpp"

#include <iostream>

TestSuite::TestSuite(DBus::Connection &connection)
    : DBus::ObjectAdaptor(connection, "/com/reverendhomer/TestSuite")
    , m_Connection(connection)
{}

std::string TestSuite::AddCon(const std::string &suffix)
{
    std::string path = "/com/reverendhomer/Testers/" + suffix;
    m_Connection.request_name("com.reverendhomer.Tester");
    m_Workers.emplace_back(m_Connection, path);
    return path;
}

///

Tester::Tester(DBus::Connection &connection, const std::string &path)
    : DBus::ObjectAdaptor(connection, path)
{
    Foo = "pepyaka";
}

std::string Tester::Prints()
{
    return "ololo " + Foo();
}

void Tester::on_set_property(DBus::InterfaceAdaptor &interface,
                             const std::string &property,
                             const DBus::Variant &value)
{
    std::cerr << "ololo set property" << std::endl;
}

DBus::BusDispatcher dispatcher;

int main()
{
    DBus::default_dispatcher = &dispatcher;
    DBus::Connection bus = DBus::Connection::SessionBus();
    try {
        bus.request_name("com.reverendhomer.TestSuite");
        TestSuite t(bus);
        dispatcher.enter();
    } catch (DBus::Error &e) {
        std::cerr << "cannot start service: " << e.what() << std::endl;
        return 1;
    }
    return 0;
}
