#include <stdio.h>
#include <gtk/gtk.h>

#include "g-generated.h"

static gboolean
on_add_con(TestSuite *suite,
           GDBusMethodInvocation *invocation,
           gchar *suffix,
           gpointer user_data)
{
    gchar *r;
    g_print("AddCon: suffix = %s\n", suffix);
    r = g_strdup_printf("/com/reverendhomer/Test/%s", suffix);
    test_suite_complete_add_con(suite, invocation, r);
    g_free(r);

    return TRUE;
}

static void
on_bus_acquired(GDBusConnection *connection,
                const gchar *name,
                gpointer user_data)
{
    GDBusObjectManagerServer *m;
    TestSuite *t;
    ObjectSkeleton *o;

    g_print("acquired a message bus connection\n");

    m = g_dbus_object_manager_server_new("/com/reverendhomer/TestSuite");


    o = object_skeleton_new("/com/reverendhomer/TestSuite/TestSuite1");
    t = test_suite_skeleton_new();
    g_signal_connect(t, "handle-add-con", G_CALLBACK(on_add_con), NULL);
    object_skeleton_set_test_suite(o, t);
    g_warning("unref t\n");
    g_object_unref(t);
    g_dbus_object_manager_server_export(m, G_DBUS_OBJECT_SKELETON(o));
    g_warning("unref o\n");
    g_object_unref(o);

    g_dbus_object_manager_server_set_connection(m, connection);
}

static void
on_name_acquired(GDBusConnection *connection,
                 const gchar *name,
                 gpointer user_data)
{
    g_print("acquired the name %s\n", name);
}

static void
on_name_lost(GDBusConnection *connection,
             const gchar *name,
             gpointer user_data)
{
    g_print("lost the name %s\n", name);
}

gint
main(gint argc, gchar *argv[])
{
    GMainLoop *loop;
    guint id;

    loop = g_main_loop_new(NULL, FALSE);

    id = g_bus_own_name(G_BUS_TYPE_SESSION,
                        "com.reverendhomer.TestSuite",
                        G_BUS_NAME_OWNER_FLAGS_ALLOW_REPLACEMENT |
                        G_BUS_NAME_OWNER_FLAGS_REPLACE,
                        on_bus_acquired,
                        on_name_acquired,
                        on_name_lost,
                        loop,
                        NULL);

    g_main_loop_run(loop);
    g_bus_unown_name(id);
    g_warning("unref loop\n");
    g_main_loop_unref(loop);

    return 0;
}
