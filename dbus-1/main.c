#include "dbus.h"
#include "poll.h"

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


struct daemon_t {
    struct dbus_t *dbus;
    struct mpx_t *mpx;
};


int main(void)
{
    int r;
    struct daemon_t *daemon;
    struct dbus_t *dbus;
    struct mpx_t *mpx;

    daemon = malloc(sizeof(struct daemon_t));
    assert(daemon);

    dbus = daemon->dbus = dbus_t_new();
    mpx = daemon->mpx = mpx_new();

    dbus->path = "/com/reverendhomer/Test";
    dbus->name = "com.reverendhomer.Test";

    r = dbus_t_init(dbus);
    if (r < 0)
        return 1;

    while (true) {
        dbus_t_set_listeners(dbus, mpx);
        if (do_poll(mpx, 250) < 0)
            continue;
        dbus_t_check_listeners(dbus, mpx);
    }

    mpx_destroy(mpx);
    dbus_t_destroy(dbus);
    free(daemon);
    return 0;
}
