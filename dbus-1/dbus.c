#define _GNU_SOURCE
#include "dbus.h"

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* forward declaration for watch functions helpers */
static dbus_bool_t add_watch(DBusWatch *watch, void *data);
static void remove_watch(DBusWatch *watch, void *data);

/* some utility functions for sanity check */
static size_t list_size(struct watch_t *w);

static const char *introspection_xml_template =
    "<!DOCTYPE node PUBLIC \"-//freedesktop//DTD D-BUS Object Introspection 1.0//EN\"\n"
    "\"http://www.freedesktop.org/standards/dbus/1.0/introspect.dtd\">\n"
    "<node name=\"%s\">\n"
    "  <interface name=\"org.freedesktop.DBus.Introspectable\">\n"
    "    <method name=\"Introspect\">\n"
    "      <arg name=\"data\" direction=\"out\" type=\"s\" />\n"
    "    </method>\n"
    "  </interface>\n"
    "  <interface name=\"%s\">\n"
    "    <method name=\"Foo\">\n"
    "      <arg name=\"name\" direction=\"in\" type=\"s\" />\n"
    "      <arg name=\"greeting\" direction=\"out\" type=\"s\" />\n"
    "    </method>\n"
    "    <method name=\"Bar\">\n"
    "      <arg name=\"out\" direction=\"out\" type=\"sbu\" />\n"
    "    </method>\n"
    "  </interface>\n"
    "</node>";


struct dbus_t *dbus_t_new(void)
{
    struct dbus_t *r;

    r = malloc(sizeof(struct dbus_t));
    assert(r);

    r->watches = NULL;

    r->connection = NULL;
    r->name = NULL;
    r->path = NULL;

    return r;
}

void dbus_t_destroy(struct dbus_t *dbus)
{
    /* TODO: proper cleaning */

    free(dbus);
}

int dbus_t_init(struct dbus_t *dbus)
{
    DBusConnection *c;
    DBusError err;
    DBusObjectPathVTable vtable = {NULL, &dbus_t_handler, NULL, NULL, NULL, NULL};

    dbus_error_init(&err);

    c = dbus_bus_get(DBUS_BUS_SESSION, &err);
    if (dbus_error_is_set(&err)) {
        fprintf(stderr, "connection error: %s\n", err.message);
        dbus_error_free(&err);
    }
    if (!c) {
        fprintf(stderr, "no connection\n");
        return -EACCES;
    }

    dbus_connection_set_exit_on_disconnect(c, TRUE);
    dbus_connection_set_watch_functions(c,
                                        add_watch,
                                        remove_watch,
                                        NULL,
                                        dbus,
                                        NULL);

    dbus_bus_request_name(c, dbus->name, 0, &err);
    if (dbus_error_is_set(&err)) {
        fprintf(stderr, "request name error: %s\n", err.message);
        dbus_error_free(&err);
        return -EACCES;
    }

    if (!dbus_connection_register_object_path(c, dbus->path, &vtable, dbus)) {
        fprintf(stderr, "cannot register a DBus message handler\n");
        return -EACCES;
    }

    dbus->connection = c;

    return 0;
}

DBusHandlerResult dbus_t_handler(DBusConnection *c,
                                 DBusMessage *m,
                                 void *user_data)
{
    struct dbus_t *dbus = user_data;
    DBusMessage *reply = NULL;
    char *method;

    printf("handler!\n");
    if (dbus_message_is_method_call(m, DBUS_INTERFACE_INTROSPECTABLE, "Introspect")) {
        char *introspection_xml;

        reply = dbus_message_new_method_return(m);

        asprintf(&introspection_xml, introspection_xml_template,
                 dbus->path, dbus->name);

        dbus_message_append_args(reply,
                                 DBUS_TYPE_STRING,
                                 &introspection_xml,
                                 DBUS_TYPE_INVALID);

        dbus_connection_send(c, reply, NULL);

        dbus_message_unref(reply);
        free(introspection_xml);

        return DBUS_HANDLER_RESULT_HANDLED;
    }

    method = (char*)dbus_message_get_member(m);
    if (!strcmp(method, "Foo"))
        reply = foo_handler(m);
    else if (!strcmp(method, "Bar"))
        reply = bar_handler(m);
    else
        return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;

    if (!reply)
        reply = dbus_message_new_method_return(m);

    if (reply) {
        dbus_connection_send(c, reply, NULL);
        dbus_message_unref(reply);
    }

    return DBUS_HANDLER_RESULT_HANDLED;
}

void dbus_t_set_listeners(struct dbus_t *dbus, struct mpx_t *mpx)
{
    for (struct watch_t *w = dbus->watches; w; w = w->next) {
        if (dbus_watch_get_enabled(w->watch)) {
            unsigned flags = dbus_watch_get_flags(w->watch);
            int fd = dbus_watch_get_unix_fd(w->watch);

            if (flags & DBUS_WATCH_READABLE)
                poll_listen(mpx, fd, POLLIN);

            if (flags & DBUS_WATCH_WRITABLE)
                poll_listen(mpx, fd, POLLOUT);

            poll_listen(mpx, fd, POLLERR);
        }
    }
}

void dbus_t_check_listeners(struct dbus_t *dbus, struct mpx_t *mpx)
{
    DBusConnection *c = dbus->connection;

    for (struct watch_t *w = dbus->watches; w; w = w->next) {
        if (dbus_watch_get_enabled(w->watch)) {
            unsigned flags = 0;
            int fd = dbus_watch_get_unix_fd(w->watch);

            if (poll_check(mpx, fd, POLLIN))
                flags |= DBUS_WATCH_READABLE;

            if (poll_check(mpx, fd, POLLOUT))
                flags |= DBUS_WATCH_WRITABLE;

            if (poll_check(mpx, fd, POLLERR))
                flags |= DBUS_WATCH_ERROR;

            if (flags)
                dbus_watch_handle(w->watch, flags);
        }
    }

    if (c) {
        dbus_connection_ref(c);
        while (dbus_connection_dispatch(c) == DBUS_DISPATCH_DATA_REMAINS) ;
        dbus_connection_unref(c);
    }
}

DBusMessage *foo_handler(DBusMessage *m)
{
    DBusMessageIter args, ret_args;
    DBusMessage *reply;
    char *greet;
    const char *str = NULL;

    if (!dbus_message_iter_init(m, &args))
        return dbus_message_new_error(m, DBUS_ERROR_INVALID_ARGS,
                                      "failed to initialize dbus message iter");

    if (dbus_message_iter_get_arg_type(&args) != DBUS_TYPE_STRING)
        return dbus_message_new_error(m, DBUS_ERROR_INVALID_ARGS,
                                      "expected string argument");

    dbus_message_iter_get_basic(&args, &str);
    if (!str || !strlen(str))
        return dbus_message_new_error(m, DBUS_ERROR_INVALID_ARGS,
                                      "empty string");

    if (strcmp(str, "foo"))
        return dbus_message_new_error_printf(
            m, DBUS_ERROR_INVALID_ARGS,
            "\"%s\" is invalid arg, only \"foo\" is acceptable", str);

    asprintf(&greet, "Hello from Foo with \"%s\"", str);

    reply = dbus_message_new_method_return(m);
    dbus_message_iter_init_append(reply, &ret_args);
    dbus_message_iter_append_basic(&ret_args, DBUS_TYPE_STRING, &greet);

    free(greet);

    return reply;
}

DBusMessage *bar_handler(DBusMessage *m)
{
    DBusMessageIter args;
    DBusMessage *r;
    const char *s = "hello world!";
    dbus_bool_t b = TRUE;
    dbus_uint32_t u = 42;

    r = dbus_message_new_method_return(m);

    dbus_message_iter_init_append(r, &args);
    dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &s);
    dbus_message_iter_append_basic(&args, DBUS_TYPE_BOOLEAN, &b);
    dbus_message_iter_append_basic(&args, DBUS_TYPE_UINT32, &u);

    return r;
}

/* callbacks for dbus_connection_set_watch_functions */
static dbus_bool_t add_watch(DBusWatch *watch, void *data)
{
    struct watch_t *w;
    struct dbus_t *dbus = data;

    for (w = dbus->watches; w; w = w->next) {
        if (w->watch == watch)
            return TRUE;
    }

    printf("add new watch\n");
    w = malloc(sizeof(struct watch_t));
    if (!w)
        return FALSE;

    w->watch = watch;
    w->next = dbus->watches;
    dbus->watches = w;

    printf("after all size of list=%zu\n", list_size(dbus->watches));

    return TRUE;
}

static void remove_watch(DBusWatch *watch, void *data)
{
    struct watch_t **up, *w, *tmp;
    struct dbus_t *dbus = data;
    printf("remove watch\n");

    for (up = &(dbus->watches), w = dbus->watches; w; w = tmp) {
        tmp = tmp->next;
        if (w->watch == watch) {
            *up = tmp;
            free(w);
        } else {
            up = &(w->next);
        }
    }
}

static size_t list_size(struct watch_t *w)
{
    size_t r = 0;
    for (struct watch_t *i = w; i; i = i->next)
        ++r;

    return r;
}
