#pragma once

#include "poll.h"

#include <dbus/dbus.h>

struct watch_t {
    DBusWatch *watch;
    struct watch_t *next;
};

struct dbus_t {
    DBusConnection *connection;
    const char *name;
    const char *path;
    struct watch_t *watches;
};

/* dbus functions */
struct dbus_t *dbus_t_new(void);
void dbus_t_destroy(struct dbus_t *dbus);
int dbus_t_init(struct dbus_t *dbus);
DBusHandlerResult dbus_t_handler(DBusConnection *c,
                                 DBusMessage *m,
                                 void *user_data);

void dbus_t_set_listeners(struct dbus_t *dbus, struct mpx_t *mpx);
void dbus_t_check_listeners(struct dbus_t *dbus, struct mpx_t *mpx);

/* method handlers */
DBusMessage *foo_handler(DBusMessage *m);
DBusMessage *bar_handler(DBusMessage *m);
