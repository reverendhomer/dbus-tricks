#include "poll.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

struct mpx_t *mpx_new(void)
{
    struct mpx_t *r = malloc(sizeof(struct mpx_t));

    r->nfds = r->pollfds_size = 0;
    r->pollfds = NULL;

    return r;
}

void mpx_destroy(struct mpx_t *mpx)
{
    free(mpx->pollfds);
    free(mpx);
}

nfds_t fd_search(struct mpx_t *mpx, int fd)
{
    nfds_t l, r, m;

    if (!(r = mpx->nfds))
        return 0;

    l = 0;

    while (true) {
        if (r == l + 1)
            return (mpx->pollfds[l].fd >= fd) ? l : r;

        m = (r + l) / 2;

        if (mpx->pollfds[m].fd > fd)
            r = m;
        else
            l = m;
    }
}

int poll_check(struct mpx_t *mpx, int fd, short event)
{
    nfds_t i = fd_search(mpx, fd);

    if (i < mpx->nfds && mpx->pollfds[i].fd == fd)
        return mpx->pollfds[i].revents & event;

    return 0;
}

void poll_listen(struct mpx_t *mpx, int fd, short event)
{
    nfds_t i = fd_search(mpx, fd);

    if (i < mpx->nfds && mpx->pollfds[i].fd == fd) {
        mpx->pollfds[i].events |= event;
        return;
    }

    if (mpx->pollfds_size != mpx->nfds) {
        memmove(&(mpx->pollfds[i + 1]),
                &(mpx->pollfds[i]),
                (mpx->nfds - i) * sizeof(struct pollfd));
    } else {
        struct pollfd *n;

        mpx->pollfds_size = (mpx->pollfds_size == 0) ? 64 : mpx->pollfds_size * 2;

        n = malloc(mpx->pollfds_size * sizeof(struct pollfd));
        assert(n);

        if (mpx->pollfds) {
            memcpy(n, mpx->pollfds, i * sizeof(struct pollfd));
            memcpy(&n[i + 1],
                   &(mpx->pollfds[i]),
                   (mpx->nfds - i) * sizeof(struct pollfd));

            free(mpx->pollfds);
        }

        mpx->pollfds = n;
    }

    mpx->pollfds[i].fd = fd;
    mpx->pollfds[i].events = event;
    mpx->nfds++;
}

int do_poll(struct mpx_t *mpx, int timeout)
{
    return poll(mpx->pollfds, mpx->nfds, timeout);
}
