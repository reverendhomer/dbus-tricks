#pragma once

#include <poll.h>

struct mpx_t {
    nfds_t nfds;
    nfds_t pollfds_size;
    struct pollfd *pollfds;
};

struct mpx_t *mpx_new(void);
void mpx_destroy(struct mpx_t *mpx);

nfds_t fd_search(struct mpx_t *mpx, int fd);
int poll_check(struct mpx_t *mpx, int fd, short event);
void poll_listen(struct mpx_t *mpx, int fd, short event);
int do_poll(struct mpx_t *mpx, int timeout);
